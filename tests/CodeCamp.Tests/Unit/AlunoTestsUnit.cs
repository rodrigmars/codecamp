using CodeCamp.Domain.Entities;
using System;
using System.Collections.Generic;
using Xunit;

namespace CodeCamp.Tests.Unit
{
    public class AlunoTestsUnit
    {
        [Fact]
        public void ListarAlunosOk()
        {
            // Fixture setup
            var alunosMoq = new List<Aluno>
            {
                new Aluno(1, "Maria teste ", DateTime.Parse("09/08/1985"), new List<AlunosMaterias>(), 5),
                new Aluno(1, "Lucas Diego Bruno Silva", DateTime.Parse("09/08/1978"), new List<AlunosMaterias>{
                    new AlunosMaterias(1, 1, 7),
                    new AlunosMaterias(1, 2, 4),
                    new AlunosMaterias(1, 3, 8),
                    new AlunosMaterias(1, 4, 7),
                    new AlunosMaterias(1, 5, 3)
                }),
                new Aluno(2, "Luana Manuela Esther da Concei��o", DateTime.Parse("21/01/1961"),  new List<AlunosMaterias>{
                    new AlunosMaterias(2, 1, 5),
                    new AlunosMaterias(2, 2, 10),
                    new AlunosMaterias(2, 3, 10),
                    new AlunosMaterias(2, 4, 7),
                    new AlunosMaterias(2, 5, 8)
                }),
                new Aluno(3, "Kamilly Stella Manuela da Mota", DateTime.Parse("22/08/1972"),  new List<AlunosMaterias>{
                    new AlunosMaterias(3, 1, 9),
                    new AlunosMaterias(3, 2, 9),
                    new AlunosMaterias(3, 3, 5),
                    new AlunosMaterias(3, 4, 5),
                    new AlunosMaterias(3, 5, 7)
                }),
                new Aluno(4, "Cristiane Al�cia Assun��o", DateTime.Parse("05/07/1981"),  new List<AlunosMaterias>{
                    new AlunosMaterias(4, 1, 8),
                    new AlunosMaterias(4, 2, 8),
                    new AlunosMaterias(4, 3, 5),
                    new AlunosMaterias(4, 4, 4),
                    new AlunosMaterias(4, 5, 7)
                }),
                new Aluno(5, "Rafael Raul Moreira", DateTime.Parse("05/07/1981"),  new List<AlunosMaterias>{
                    new AlunosMaterias(5, 1, 5),
                    new AlunosMaterias(5, 2, 4),
                    new AlunosMaterias(5, 3, 8),
                    new AlunosMaterias(5, 4, 9),
                    new AlunosMaterias(5, 5, 7)
                }),
            };

            //System under test(SUT)
            var sut = new Aluno();

            // Exercise system
            var aprovados = sut.ListaAprovados(alunosMoq);

            // Verify outcome
            Assert.All(aprovados, aprovado => Assert.True(aprovado.Media >= 7));

            // Teardown
        }
    }
}
