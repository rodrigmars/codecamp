﻿namespace CodeCamp.Domain.Entities
{
    public class AlunosMaterias
    {
        public AlunosMaterias(int alunoId, int materiaId, double nota)
        {
            AlunoId = alunoId;

            MateriaId = materiaId;

            Nota = nota;
        }

        public int AlunoId { get; private set; }

        public int MateriaId { get; private set; }

        public double Nota { get; private set; }
    }
}
