﻿namespace CodeCamp.Domain.Entities
{
    public class Materia: Entity
    {
        public Materia()
        {

        }

        public Materia(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        public string Nome { get; private set; }
    }
}