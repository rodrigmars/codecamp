﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeCamp.Domain.Entities
{
    public class Aluno : Entity
    {
        public Aluno()
        {

        }

        public Aluno(int id, string nome, DateTime dataNascimento, IList<AlunosMaterias> alunosMaterias = null, double media = 0)
        {
            Id = id;
            Nome = nome;
            DataNascimento = dataNascimento;
            Idade = CalculaIdade();

            Media = (alunosMaterias.Any()) ?
                CalculaMedia(alunosMaterias.Select(x => x.Nota).Sum(),
                alunosMaterias.Select(x => x.Nota).Count()) : media;

            AlunosMaterias = alunosMaterias;
        }

        public string Nome { get; private set; }

        public DateTime DataNascimento { get; private set; }

        public int Idade { get; private set; }

        public IList<AlunosMaterias> AlunosMaterias { get; private set; }

        public double Media { get; private set; }

        /// <summary>
        /// Calcula média e retorna os alunos aprovados
        /// </summary>
        /// <param name="alunos"></param>
        /// <returns>Lista de alunos</returns>
        public IEnumerable<Aluno> ListaAprovados(IList<Aluno> alunos)
        {
            foreach (var aluno in alunos)
            {
                if (aluno.Media >= 7)
                {
                    yield return aluno;
                }
            }
        }

        /// <summary>
        /// Calula média do aluno
        /// </summary>
        /// <param name="total"></param>
        /// <param name="numero"></param>
        /// <returns>Média calculada</returns>
        private double CalculaMedia(double total, int numero) => total / numero;

        /// <summary>
        /// Calcula idade do aluno
        /// </summary>
        /// <returns>idade calculada</returns>
        private int CalculaIdade()
        {
            int idade = DateTime.Now.Year - DataNascimento.Year;

            if (DateTime.Now.Month < DataNascimento.Month || (DateTime.Now.Month == DataNascimento.Month && DateTime.Now.Day < DataNascimento.Day))
            {
                idade--;
            }

            return idade;
        }
    }
}